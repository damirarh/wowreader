#Sample RSS Reader Application

This a sample application that I have written for my Windows Store apps development workshop. It is a simple modification of the basic Grid App project template that can be live coded in a little over an hour.

In spite of its simplicity it includes support for several Windows Store app specific features:

- Settings charm
- Share charm
- Search charm

There's no accompanying documentation but it should be quite easy to comprehend.