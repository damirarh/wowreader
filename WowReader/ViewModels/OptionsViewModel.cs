﻿using RelayCommandRt;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using WowReader.Common;

namespace WowReader.ViewModels
{
    public class OptionsViewModel : BindableBase
    {
        private string _NewUrl;
        public string NewUrl
        {
            get
            {
                return _NewUrl;
            }
            set
            {
                if (_NewUrl == value)
                    return;
                _NewUrl = value;
                OnPropertyChanged();
            }
        }
        public string CurrentUrl { get; set; }
        public ObservableCollection<string> Feeds { get; set; }

        public RelayCommand AddCommand { get; set; }
        public RelayCommand RemoveCommand { get; set; }

        public OptionsViewModel()
        {
            Load();

            AddCommand = new RelayCommand(_ => OnAdd());
            RemoveCommand = new RelayCommand(_ => OnRemove());
        }

        private void OnRemove()
        {
            Feeds.Remove(CurrentUrl);
            Save();
        }

        private void OnAdd()
        {
            Feeds.Add(NewUrl);
            NewUrl = String.Empty;
            Save();
        }

        private void Save()
        {
            ApplicationData.Current.LocalSettings.Values["Feeds"] = String.Join("|", Feeds.ToArray());
        }

        private void Load()
        {
            object value = ApplicationData.Current.LocalSettings.Values["Feeds"];
            if (value != null)
                Feeds = new ObservableCollection<string>(ApplicationData.Current.LocalSettings.Values["Feeds"].ToString().Split('|'));
            else
                Feeds = new ObservableCollection<string>();
        }
    }
}
